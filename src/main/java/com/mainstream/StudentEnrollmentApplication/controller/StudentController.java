package com.mainstream.StudentEnrollmentApplication.controller;

import com.mainstream.StudentEnrollmentApplication.model.Courses;
import com.mainstream.StudentEnrollmentApplication.model.Payment;
import com.mainstream.StudentEnrollmentApplication.model.Student;
import com.mainstream.StudentEnrollmentApplication.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/student")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @PostMapping("/enroll")
    public Student enrollStudent(@RequestBody Student student) {
        return studentService.enrollStudent(student);
    }

    @GetMapping("/all")
    public List<Student> getAllStudents() {
        return studentService.allStudent();
    }

    @GetMapping("/{studentNumber}")
    public Optional<Student> getStudentById(@PathVariable String studentNumber){
        return studentService.getStudentByStudentId(studentNumber);
    }
    @PostMapping("/courses/{studentNumber}")
    public Courses addCourse(@PathVariable("studentNumber") String studentNumber, @RequestBody Courses courses) {
        return studentService.addCourse(studentNumber, courses);
    }

    @GetMapping("/courses/{studentNumber}")
    public Double addCourse(@PathVariable("studentNumber") String studentNumber) {
        return studentService.getTotalTuition(studentNumber);
    }

    @PostMapping("/payment")
    public Payment makePayment(@RequestBody Payment payment) {
        return studentService.makePayment(payment);
    }
}
