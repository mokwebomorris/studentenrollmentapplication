package com.mainstream.StudentEnrollmentApplication.model;

public enum GradeYear {
    FRESHMAN,
    SOPHOMORE,
    JUNIOR,
    SENIOR
}
