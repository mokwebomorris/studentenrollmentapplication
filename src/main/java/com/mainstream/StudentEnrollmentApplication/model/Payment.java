package com.mainstream.StudentEnrollmentApplication.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "payments")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "student_number")
    private String studentId;

    @Column(name = "amount_paid")
    private double amount;

    @Column(name = "date_payment_made",updatable = false)
    @JsonIgnore
    private Date datePaymentMade;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDatePaymentMade() {
        return datePaymentMade;
    }

    public void setDatePaymentMade(Date datePaymentMade) {
        this.datePaymentMade = datePaymentMade;
    }
}
