package com.mainstream.StudentEnrollmentApplication.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "student_number")
    private String studentId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "grade_year")
    @Enumerated(EnumType.STRING)
    private GradeYear gradeYear;

    @Column(name = "tuition_balance")
    private double tuitionBalance;

    @Column(name = "courses")
    @OneToMany(mappedBy = "student")
    private List<Courses> courses;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public GradeYear getGradeYear() {
        return gradeYear;
    }

    public void setGradeYear(GradeYear gradeYear) {
        this.gradeYear = gradeYear;
    }

    public double getTuitionBalance() {
        return tuitionBalance;
    }

    public void setTuitionBalance(double tuitionBalance) {
        this.tuitionBalance = tuitionBalance;
    }

    public List<Courses> getCourses() {
        return courses;
    }

    public void setCourses(List<Courses> courses) {
        this.courses = courses;
    }
}
