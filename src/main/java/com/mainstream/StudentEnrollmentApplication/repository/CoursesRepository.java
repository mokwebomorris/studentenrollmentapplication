package com.mainstream.StudentEnrollmentApplication.repository;

import com.mainstream.StudentEnrollmentApplication.model.Courses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoursesRepository extends JpaRepository<Courses, Long> {
}
