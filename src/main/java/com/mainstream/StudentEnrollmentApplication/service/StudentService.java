package com.mainstream.StudentEnrollmentApplication.service;

import com.mainstream.StudentEnrollmentApplication.model.Courses;
import com.mainstream.StudentEnrollmentApplication.model.GradeYear;
import com.mainstream.StudentEnrollmentApplication.model.Payment;
import com.mainstream.StudentEnrollmentApplication.model.Student;
import com.mainstream.StudentEnrollmentApplication.repository.CoursesRepository;
import com.mainstream.StudentEnrollmentApplication.repository.PaymentRepository;
import com.mainstream.StudentEnrollmentApplication.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    private static double costOfCourse = 600.00;

    private static double tuitionWhenEnrolling = 0.0;
    private final StudentRepository studentRepository;
    private final CoursesRepository coursesRepository;

    private final PaymentRepository paymentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository, CoursesRepository coursesRepository, PaymentRepository paymentRepository) {
        this.studentRepository = studentRepository;
        this.coursesRepository = coursesRepository;
        this.paymentRepository = paymentRepository;
    }

    public Student enrollStudent(Student student) {

        Optional<Student> existingStudent = studentRepository.findById(student.getId());

        if (existingStudent.isPresent()) {

            throw new RuntimeException("Student with student number: " + student.getStudentId() + " does not exist");

        }

        if (student.getGradeYear().equals(GradeYear.FRESHMAN) || student.getGradeYear().equals(GradeYear.SOPHOMORE) || student.getGradeYear().equals(GradeYear.JUNIOR) || student.getGradeYear().equals(GradeYear.SENIOR)) {
            //"F123566"

            String studentNumber = student.getGradeYear().name().substring(0, 1).toUpperCase() + "" + generateStudentNumber();
            student.setStudentId(studentNumber);

            student.setTuitionBalance(tuitionWhenEnrolling);


        }
        return studentRepository.save(student);
    }

    public List<Student> allStudent() {

        return studentRepository.findAll();
    }

    public Optional<Student> getStudentByStudentId(String studentNumber){

        Optional<Student> existingStudent = Optional.ofNullable(studentRepository.findByStudentId(studentNumber));


        if(!existingStudent.isPresent()){
            throw new RuntimeException("Student with student number: " + studentNumber + " does not exist");
        }
        return existingStudent;

    }



    public Courses addCourse(String studentNumber, Courses courses) {
        Optional<Student> existingStudent = Optional.ofNullable(studentRepository.findByStudentId(studentNumber));

        if (!existingStudent.isPresent()) {
            throw new RuntimeException("Student with student number: " + studentNumber + " does not exist");
        }


        Student student = existingStudent.get();
        courses.setStudent(student);
        courses.setCourseCost(costOfCourse);

        updateTuition(student);


        return coursesRepository.save(courses);

    }

    private void updateTuition(Student student) {

        List<Courses> existingCourses = coursesRepository.findAll();
        double currentCourseAmount = 0;


        if (!existingCourses.isEmpty()) {
            //if causes is not empty we call the calculateTuition method--still working on this
            //its not counting the first added course
            System.out.println("in not empty: " + calculateTuition());

            for (Courses courses : existingCourses) {
                currentCourseAmount = student.getTuitionBalance() + courses.getCourseCost();
            }
            student.setTuitionBalance(currentCourseAmount);

        } else {
            //if there is no courses then we set the the tuition balance to the the amount of the first courses added
            System.out.println("in else  " + costOfCourse);
            student.setTuitionBalance(costOfCourse);
        }


        studentRepository.save(student);

    }

    public Payment makePayment(Payment payment) {
        double paymentMade = 0;
        Optional<Student> existingStudent = Optional.ofNullable(studentRepository.findByStudentId(payment.getStudentId()));

        if (!existingStudent.isPresent()) {
            throw new RuntimeException("Student with student number: " + payment.getStudentId() + " does not exist");
        }

        Student student = existingStudent.get();
        paymentMade = (student.getTuitionBalance() - payment.getAmount());
        student.setTuitionBalance(paymentMade);

        payment.setDatePaymentMade(new Date());
        return paymentRepository.save(payment);

    }


    //still working on this:so far so good
    public double getTotalTuition(String studentNumber) {
        Optional<Student> existingStudent = Optional.ofNullable(studentRepository.findByStudentId(studentNumber));

        if (!existingStudent.isPresent()) {
            throw new RuntimeException("Student with student number: " + studentNumber + " does not exist");
        }

        return calculateTuition();
    }

    private double calculateTuition() {
        double totalCost = 0.00;

        List<Courses> allCourses = coursesRepository.findAll();

        for (Courses courses : allCourses) {
            totalCost = totalCost + courses.getCourseCost();
        }
        return totalCost;

    }




/*    public List<Courses> addMultipleCourse(String studentNumber, List<Courses> courses){
        Optional<Student> existingStudent = Optional.ofNullable(studentRepository.findByStudentId(studentNumber));
        if(!existingStudent.isPresent()){
            throw  new RuntimeException("Student with student number: "+studentNumber+" does not exist");
        }

        Student student = existingStudent.get();
        return coursesRepository.saveAll(courses);
    }*/

    private int generateStudentNumber() {
        final int min = 100000;
        final int max = 900000;
        int studentNo = (int) Math.floor(Math.random() * (max - min + 1) + min);
        return studentNo;
    }


}
